import axios from 'axios';

const instance =axios.create({
    baseURL: 'https://burger-junusova.firebaseio.com/'
});

instance.interceptors.request.use(reg =>{
    console.log(['In request interseptor', reg]);
    return reg;
})


export default instance;