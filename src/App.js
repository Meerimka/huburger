import React, { Component } from 'react';
import BurgerBuilder from "./containers/BurgerBuilder/BurgerBuilder";
import {Route,Switch} from 'react-router-dom';
import Checkout from "./containers/Checkout/Checkout";
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";
import Counter from "./containers/Counter/Counter";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={BurgerBuilder}/>
                <Route path="/checkout" component={Checkout} />
                <Route path="/orders" component={Orders} />
                <Route path="/counter" component={Counter} />
                <Route render={()=><h1>Not Found!</h1>} />


        </Switch>
        </Layout>

    );
  }
}

export default App;
