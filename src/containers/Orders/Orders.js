import React, {Component} from 'react';
import axios from '../../axios-orders';
import OrderItem from "../../components/Order/OrderItem/OrderItem";
import WithErrorHandle from "../../hoc/withErrorHandle";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";

class Orders extends Component {
    state={
        orders:[],
        loading:true,
    };

    componentDidMount(){
        axios.get('/orders.json').then(response =>{
            const orders=Object.keys(response.data).map(id =>{
                return {
                    ...response.data[id],
                    id
                };
            });

            this.setState({orders})
        }).finally(()=>{
            this.setState({loading:false})
        })
    }

    render() {
        if(this.state.loading){
            return <div>....</div>
        }
        return this.state.orders.map(order=>(
            <ErrorBoundary  key={order.id}>
            <OrderItem
            ingredients={order.ingredients}
            price={order.price}
            />
            </ErrorBoundary>
        ));
    }
}

export default WithErrorHandle(Orders, axios) ;