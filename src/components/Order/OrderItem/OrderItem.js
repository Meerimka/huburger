import React from 'react';
import './OrderItem.css'

const OrderItem = ({ingredients, price}) => {

    const IngredientsOutput =Object.keys(ingredients).map(igName=>(
        <span key={igName} >{igName} ({ingredients[igName]})</span>
    ))


    if(Math.random()>0.7) throw new  Error('Well, this happened');


    return (
        <div className="OrderItem">
            <p>Ingredients: {IngredientsOutput}</p>
            <p> Price: <strong>{} KGS</strong></p>

        </div>
    );
};

export default OrderItem;