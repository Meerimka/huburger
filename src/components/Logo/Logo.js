import React from 'react';
import './Logo.css';
import burgerLogo from '../../assets/burger_logo.png';

const Logo = () => {
    return (
        <div className="Logo">
            <img src={burgerLogo} alt="burgerLogo"/>
        </div>
    );
};

export default Logo;