import React, {Component, Fragment} from 'react';
import Modal from "../components/UI/Modal/Modal";


const WithErrorHandle = (WrappedComponents, axios)=> {
    return class WithErrorHandlerHOC extends Component {
        constructor(props){
            super(props);
             this.state={
                 error:null,
             } ;

            this.state.interceptorId = axios.interceptors.response.use(res => res ,error =>{
                this.setState({error});
                throw error;
            });
        };

        errordismissed = () =>{
            this.setState({error: null})
        };

        componentWillUnmount(){
            axios.interceptors.response.eject(this.state.interceptorId);
        }

        render(){

            return (
                <Fragment>
                    <Modal show={this.state.error} close={this.errordismissed}>
                        {/*{this.state.error ? this.state.error.message :null}*/}
                        {this.state.error && this.state.error.message}
                    </Modal>
                <WrappedComponents {...this.props}/>
                </Fragment>
            )
        }
    }
};

export default WithErrorHandle;